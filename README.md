# TestCppTango867

## Description

This project was created to reproduce the problem identified in [cppTango#867](https://gitlab.com/tango-controls/cppTango/-/issues/867).  
To reproduce the issue, you'll need to patch the cppTango library to add a sleep of 1 second in 1 method to greatly increase the probability to reproduce the race condition issue.  
The device server provided in this repository is creating a thread during the init_device sequence and this thread is creating a group of 1000 devices repeatedly in a loop.

## How to compile?

Open TestCppTango867.xmi with Pogo and regenerate the Makefile or CMakeLists.txt for your installation.

### Using CMake

```
git clone https://gitlab.com/bourtemb/testcpptango867.git

cd testcpptango867

mkdir build

cd build

cmake ..

make -j

```

### Using Makefile

```

make -j

```

## Steps to reproduce [cppTango#867](https://gitlab.com/tango-controls/cppTango/-/issues/867)

1. Ensure you have TANGO_HOST environment variable correctly defined for your TANGO installation.

1. Create a TestCppTango867/test device server instance in your Tango Database with 1 device.
    ```
    tango_admin --add-server TestCppTango867/test TestCppTango867 test/cpptango/867
    ```

1. Apply the [following patch for the main cppTango branch](0001-Add-sleep-time-in-DbServerCache-import_tac_dev-main.patch) or [this one for the 9.3-backports branch](0001-Add-sleep-time-in-DbServerCache-import_tac_dev-9.3.patch) to cppTango to increase the probability to reproduce the concurrency issue.  
The patch adds a sleep of 1 second in DbServerCache::import_tac_dev() method.

    ```
    cd ../..
    git clone git@gitlab.com:tango-controls/cppTango.git cppTango-Test-Issue-867  (or git clone https://gitlab.com/tango-controls/cppTango.git cppTango-Test-Issue-867)  
    cd cppTango-Test-Issue-867  
    ```

    If you want to test on cppTango main branch:

    ```
    git checkout main
    git apply ../0001-Add-sleep-time-in-DbServerCache-import_tac_dev-main.patch
    ```

    If you want to test on cppTango 9.3-backports branch:

    ```
    git checkout 9.3-backports
    git apply ../0001-Add-sleep-time-in-DbServerCache-import_tac_dev-9.3.patch
    ```
    Build and install this modified version of cppTango:
    ```
    mkdir build
    cd build
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=/tmp/install/cpptango-test-issue-867 ..
    make -j install
    ```

1. Adapt your LD_LIBRARY_PATH to use the modified cppTango version

    ```
    export LD_LIBRARY_PATH=/tmp/install/cpptango-test-issue-867/lib:$LD_LIBRARY_PATH
    ```

1. Start TestCppTango867 test instance (Assuming you are in the directory where the devie server executable was generated)
    ```
    cd ../../testcpptango867/build
    ./TestCppTango867 test -v2
    ```

1. Wait for the crash which should happen slightly after seeing `Ready to accept requests` messages
