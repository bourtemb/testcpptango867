/*----- PROTECTED REGION ID(TestCppTango867.h) ENABLED START -----*/
/* clang-format on */
//=============================================================================
//
// file :        TestCppTango867.h
//
// description : Include file for the TestCppTango867 class
//
// project :     TestCppTango867
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//
// Copyright (C): 2023
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef TestCppTango867_H
#define TestCppTango867_H

#ifdef TANGO_HAS_NEW_HEADER_LAYOUT
#include <tango/tango.h>
#else
#include <tango.h>
#endif


/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	TestCppTango867.h

#ifdef TANGO_LOG
	// cppTango after c934adea (Merge branch 'remove-cout-definition' into 'main', 2022-05-23)
	// nothing to do
#else
	// cppTango 9.3-backports and older
	#define TANGO_LOG       cout
	#define TANGO_LOG_INFO  cout2
	#define TANGO_LOG_DEBUG cout3
#endif // TANGO_LOG

/**
 *  TestCppTango867 class description:
 *    A Tango Device Class to try to reproduce the issue described in 
 *    cpptango#867.
 */


namespace TestCppTango867_ns
{
/*----- PROTECTED REGION ID(TestCppTango867::Additional Class Declarations) ENABLED START -----*/
/* clang-format on */
//	Additional Class Declarations
class MyThread;
/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	TestCppTango867::Additional Class Declarations

class TestCppTango867 : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(TestCppTango867::Data Members) ENABLED START -----*/
/* clang-format on */
//	Add your own data members
private:
	MyThread * my_thread;
/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	TestCppTango867::Data Members

//	Device property data members
public:
	//	after_init_sleep_time:	Sleep time in seconds at the end of the init_device() method
	Tango::DevULong	after_init_sleep_time;


//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	TestCppTango867(Tango::DeviceClass *cl,std::string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	TestCppTango867(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	TestCppTango867(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~TestCppTango867();


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method     : TestCppTango867::read_attr_hardware()
	 *	Description: Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(std::vector<long> &attr_list);


	//--------------------------------------------------------
	/**
	 *	Method     : TestCppTango867::add_dynamic_attributes()
	 *	Description: Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:


	//--------------------------------------------------------
	/**
	 *	Method     : TestCppTango867::add_dynamic_commands()
	 *	Description: Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(TestCppTango867::Additional Method prototypes) ENABLED START -----*/
/* clang-format on */
//	Additional Method prototypes
/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	TestCppTango867::Additional Method prototypes
};

/*----- PROTECTED REGION ID(TestCppTango867::Additional Classes Definitions) ENABLED START -----*/
/* clang-format on */
//	Additional Classes Definitions
class MyThread : public omni_thread
{
    private:
        bool needStop;           
		useconds_t startup_usleep_time;
    public:
        omni_mutex sharedData;
		std::vector<std::string> group_dev_list;
            
        MyThread();
        ~MyThread();

        void start() 
		{
                start_undetached();
        }

        void stop();

    private:
        bool threadNeedStop();

        void *run_undetached(void *ptr) override;
};
/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	TestCppTango867::Additional Classes Definitions

}	//	End of namespace

#endif   //	TestCppTango867_H
