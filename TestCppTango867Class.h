/*----- PROTECTED REGION ID(TestCppTango867Class.h) ENABLED START -----*/
/* clang-format on */
//=============================================================================
//
// file :        TestCppTango867Class.h
//
// description : Include for the TestCppTango867 root class.
//               This class is the singleton class for
//                the TestCppTango867 device class.
//               It contains all properties and methods which the
//               TestCppTango867 requires only once e.g. the commands.
//
// project :     TestCppTango867
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//
// Copyright (C): 2023
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef TestCppTango867Class_H
#define TestCppTango867Class_H

#ifdef TANGO_HAS_NEW_HEADER_LAYOUT
#include <tango/tango.h>
#else
#include <tango.h>
#endif

#include "TestCppTango867.h"

/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	TestCppTango867Class.h


namespace TestCppTango867_ns
{
/*----- PROTECTED REGION ID(TestCppTango867Class::classes for dynamic creation) ENABLED START -----*/
/* clang-format on */

/* clang-format off */
/*----- PROTECTED REGION END -----*/	//	TestCppTango867Class::classes for dynamic creation

/**
 *	The TestCppTango867Class singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  TestCppTango867Class : public Tango::DeviceClass
#else
class TestCppTango867Class : public Tango::DeviceClass
#endif
{
	/*----- PROTECTED REGION ID(TestCppTango867Class::Additional DServer data members) ENABLED START -----*/
	/* clang-format on */
	//	Add your own code
	/* clang-format off */
	/*----- PROTECTED REGION END -----*/	//	TestCppTango867Class::Additional DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
		//	Method prototypes
		static TestCppTango867Class *init(const char *);
		static TestCppTango867Class *instance();
		~TestCppTango867Class();
		Tango::DbDatum	get_class_property(std::string &);
		Tango::DbDatum	get_default_device_property(std::string &);
		Tango::DbDatum	get_default_class_property(std::string &);

	protected:
		TestCppTango867Class(std::string &);
		static TestCppTango867Class *_instance;
		void command_factory();
		void attribute_factory(std::vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		std::string get_cvstag();
		std::string get_cvsroot();

	private:
		void device_factory(TANGO_UNUSED(const Tango::DevVarStringArray *));
		void create_static_attribute_list(std::vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,std::vector<Tango::Attr *> &);
		std::vector<std::string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(std::vector<Tango::Attr *> &att_list, std::string attname);
};

}	//	End of namespace

#endif   //	TestCppTango867_H
